<?php
namespace Ikx\SMS\Command;

use Ikx\Core\Application;
use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Command\CommandInterface;
use Ikx\Core\Utils\MessagingTrait;
use Twilio\Rest\Client;

class SmsCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public function describe()
    {
        return "Send an SMS";
    }

    public function run() {
        if (isset($this->params[0]) && isset($this->params[1])) {
            $phone = $this->params[0];
            $text = [];
            for($i = 1; $i < count($this->params); $i++) {
                $text[] = $this->params[$i];
            }
            $text = implode(' ', $text);

            $sid = Application::config()->get('options.twilio.sid');
            $auth = Application::config()->get('options.twilio.auth');
            $number = Application::config()->get('options.twilio.number');

            $client = new Client($sid, $auth);
            $client->messages->create(
            // Where to send a text message (your cell phone?)
                $phone,
                array(
                    'from' => $number,
                    'body' => $text
                )
            );

            $this->msg($this->channel, "Text message has been sent.");
        }
    }
}