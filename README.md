# Ikx IRC Bot SMS module
## Configuration
The SMS module requires some configuration in order to work.
Add the following to your `app/config.yml` file in order to allow the SMS
module work.

```
options:
  twilio:
    sid: "ACXXXXXX"
    auth: "XXXXXXX"
    number: "+1234567890"
```

Replace the value of `sid` with your Twilio API SID, the value of `auth` with
your Twilio API auth key and the value of `number` with your own personal
Twilio phone number.